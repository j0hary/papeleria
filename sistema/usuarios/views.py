from django.shortcuts import render
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from usuarios.forms import UsuariosCrearForma, UsuariosModificarForma
from django.views.generic import ListView, DetailView 
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from usuarios.models import Usuarios, Almacen, Rol

from django.urls import reverse

from django.contrib import messages 
from django.contrib.messages.views import SuccessMessageMixin 

from django import forms

class UsuariosListado(ListView): 
    model = Usuarios

class UsuariosDetalle(DetailView): 
    model = Usuarios

class UsuariosCrear(SuccessMessageMixin, CreateView): 
    #model = Usuarios
    form_class = UsuariosCrearForma
    #fields = ('username', 'email', 'first_name', 'last_name', 'almacen') 
    success_message = 'Usuario Creado Correctamente !' # Mostramos este Mensaje luego de Crear un Usuario     

    # Redireccionamos a la página principal luego de crear un registro o Usuarios
    def get_success_url(self):        
        return reverse('lstusuarios') # Redireccionamos a la vista principal

class UsuariosActualizar(SuccessMessageMixin, UpdateView): 
    model = Usuarios
    #form_class = UsuariosModificarForma
    fields = ('username', 'email', 'first_name', 'last_name', 'almacen','rol')  
    success_message = 'Usuario Actualizado Correctamente !' # Mostramos este Mensaje luego de Editar un Usuario

    # Redireccionamos a la página principal luego de actualizar un registro o Usuario
    def get_success_url(self):               
        return reverse('lstusuarios') # Redireccionamos a la vista principal 'leer' 

class MiUsuariosActualizar(SuccessMessageMixin, UpdateView): 
    model = Usuarios
    #form_class = UsuariosModificarForma
    fields = ('username', 'email', 'first_name', 'last_name', 'almacen')  
    success_message = 'Usuario Actualizado Correctamente !' # Mostramos este Mensaje luego de Editar un Usuario

    # Redireccionamos a la página principal luego de actualizar un registro o Usuario
    def get_success_url(self):               
        return reverse('mainmenu') # Redireccionamos a la vista principal 'leer' 


class UsuariosEliminar(SuccessMessageMixin, DeleteView): 
    model = Usuarios 
    form = Usuarios
    fields = ('username', 'email', 'first_name', 'last_name', 'almacen','rol')

    
    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.is_superuser == 1:
            return redirect(self.success_url)
        return super().post(request, *args, **kwargs)
    

    # Redireccionamos a la página principal luego de eliminar un registro o Usuario
    def get_success_url(self): 
        success_message = 'Usuario Eliminado Correctamente !' # Mostramos este Mensaje luego de eliminar un Usuario
        messages.success (self.request, (success_message))       
        return reverse('lstusuarios') # Redireccionamos a la vista principal                   

@login_required(login_url='usuarios/login')
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            #update_session_auth_hash(request, user)  # Important!
            #messages.success(request, 'El password fue cambiado exitosamente')
            return redirect('logout')
        else:
            messages.error(request, 'Por favor corrija los errores para continuar')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'change_password.html', {'form': form})



############## vistas del modelo almacen ###################

class AlmacenListado(ListView): 
    model = Almacen

class AlmacenDetalle(DetailView): 
    model = Almacen

class AlmacenCrear(SuccessMessageMixin, CreateView): 
    model = Almacen
    #form_class = UsuariosCrearForma
    fields = ('almacen',) 
    success_message = 'Almacen Creado Correctamente !' # Mostramos este Mensaje luego de Crear un Usuario     

    # Redireccionamos a la página principal luego de crear un registro o Usuarios
    def get_success_url(self):        
        return reverse('lstalmacenes') # Redireccionamos a la vista principal

class AlmacenActualizar(SuccessMessageMixin, UpdateView): 
    model = Almacen
    #form_class = UsuariosModificarForma
    fields = ('almacen',)  
    success_message = 'Almacen Actualizado Correctamente !' # Mostramos este Mensaje luego de Editar un Usuario

    # Redireccionamos a la página principal luego de actualizar un registro o Usuario
    def get_success_url(self):               
        return reverse('lstalmacenes') # Redireccionamos a la vista principal 'leer' 

class AlmacenEliminar(SuccessMessageMixin, DeleteView): 
    model = Almacen 
    form = Almacen
    fields = ('almacen',)

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.is_superuser == 1:
            return redirect(self.success_url)
        return super().post(request, *args, **kwargs)
    
    # Redireccionamos a la página principal luego de eliminar un registro o Usuario
    def get_success_url(self): 
        success_message = 'Almacen Eliminado Correctamente !' # Mostramos este Mensaje luego de eliminar un Usuario
        messages.success (self.request, (success_message))       
        return reverse('lstalmacenes') # Redireccionamos a la vista principal                   


############################################################


############## vistas del modelo roles ###################

class RolListado(ListView): 
    model = Rol

class RolDetalle(DetailView): 
    model = Rol

class RolCrear(SuccessMessageMixin, CreateView): 
    model = Rol
    #form_class = UsuariosCrearForma
    fields = ('rol',) 
    success_message = 'Rol Creado Correctamente !' # Mostramos este Mensaje luego de Crear un Usuario     

    # Redireccionamos a la página principal luego de crear un registro o Usuarios
    def get_success_url(self):        
        return reverse('lstroles') # Redireccionamos a la vista principal

class RolActualizar(SuccessMessageMixin, UpdateView): 
    model = Rol
    #form_class = UsuariosModificarForma
    fields = ('rol',)  
    success_message = 'Rol Actualizado Correctamente !' # Mostramos este Mensaje luego de Editar un Usuario

    # Redireccionamos a la página principal luego de actualizar un registro o Usuario
    def get_success_url(self):               
        return reverse('lstroles') # Redireccionamos a la vista principal 'leer' 

class RolEliminar(SuccessMessageMixin, DeleteView): 
    model = Rol 
    form = Rol
    fields = ('rol',)

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.object.is_superuser == 1:
            return redirect(self.success_url)
        return super().post(request, *args, **kwargs)
    
    # Redireccionamos a la página principal luego de eliminar un registro o Usuario
    def get_success_url(self): 
        success_message = 'Rol Eliminado Correctamente !' # Mostramos este Mensaje luego de eliminar un Usuario
        messages.success (self.request, (success_message))       
        return reverse('lstroles') # Redireccionamos a la vista principal                   


############################################################