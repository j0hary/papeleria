from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from usuarios.models import Usuarios

class UsuariosAdmin(UserAdmin):
    add_form = UserCreationForm
    form = UserChangeForm
    model = Usuarios
    list_display = ['username', 'email', 'first_name', 'last_name',]

admin.site.register(Usuarios, UsuariosAdmin)
