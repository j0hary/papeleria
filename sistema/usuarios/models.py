from django.db import models
from django.utils import timezone

from django.contrib.auth.models import AbstractUser
from django.conf import settings
from usuarios.catalogs import ALMACENES, ROLES

class Almacen(models.Model):
    almacen = models.CharField(max_length=32,null=True, blank=True, unique=True)

    def __str__(self):
        return self.almacen

class Rol(models.Model):
    rol = models.CharField(max_length=32,null=True, blank=True, unique=True)

    def __str__(self):
        return self.rol

# Creación de campos de la tabla 'Usuarios' 
class Usuarios(AbstractUser):
    #almacen = models.CharField(max_length=30,blank=True, null=True, choices=ALMACENES)
    almacen = models.ForeignKey(Almacen, on_delete=models.CASCADE)
    #rol = models.CharField(max_length=30,blank=True, null=True, choices=ROLES)
    rol = models.ForeignKey(Rol, on_delete=models.CASCADE)

    def __str__(self):
        return self.username


User = settings.AUTH_USER_MODEL

'''
PATRON SINGLETON

Clase que controla la creacion de instancias de session, evitando el que
un usuario pueda tener mas de una sesion activa al mismo tiempo en diferentes
dispositivos/navegadores.

La clase esta vinculada con una cardinalidad 1-1 al modelo Usuarios.
Fue necesario implementarlo de esta manera ya que es mas simple que modificar
directamente la clase AbstractUser
'''
class LoggedInUser(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE, related_name='logged_in_user')
    # Session keys are 32 characters long
    session_key = models.CharField(max_length=32, null=True, blank=True)
    tiempo_expira = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.user.username
