from django.test import TestCase, Client
from django.urls import reverse
from usuarios.models import Usuarios
from usuarios.views import UsuariosListado, UsuariosDetalle, UsuariosCrear, UsuariosActualizar, UsuariosEliminar, change_password, MiUsuariosActualizar
from django.contrib.auth import authenticate
from time import sleep
from random import randint

class UsuariosTestsCase(TestCase):

    def setUp(self):
        UserList = []
        ALMACENESTEST = [1,2,3,4,5]
        ROLESTEST= [2,3,4]
        #probando creacion de un usuario administrador
        UserList.append(Usuarios.objects.create_superuser('admin', email='admin@simiconti.com', password='t35tpa55admin',almacen_id=1,rol_id=1))
        #probando creacion de N usuarios normales
        for user in range(10):
            UserList.append(Usuarios.objects.create_user('ustest'+str(user), email='usuario'+str(user)+'@simiconti.com', password='t35tpa55ustest'+str(user),almacen_id=ALMACENESTEST[randint(0,2)],rol_id=ROLESTEST[randint(0,2)]))
        for usuarioprueba in UserList:

            if(usuarioprueba.is_staff == 1):
                usuarioprueba.almacen_id = ALMACENESTEST[randint(0,4)]
                usuarioprueba.rol_id = 1
            else:    
                usuarioprueba.almacen_id = ALMACENESTEST[randint(0,4)]
                usuarioprueba.rol_id = ROLESTEST[randint(0,2)]
            usuarioprueba.save()

    def test_usuarios_login(self):
        for usuario in Usuarios.objects.all():
            c = Client()
            user = authenticate(username=usuario.username, password='t35tpa55'+usuario.username)
            if user.is_authenticated:
                print('Se autenticó correctamente al usuario: ',user)
                print(user.username)
                print(user.email)
                print(user.rol)
                print(user.almacen)
                #se realiza un request al menu principal para comprobar el login correcto
                peticion = c.post('/usuarios/login/', {'username': usuario.username, 'password': 't35tpa55'+usuario.username})
                peticion = c.get('/mainmenu')
                self.assertEqual(peticion.status_code, 200)
            else:
                print('Error al autenticar al usuario: ',usuario.username)