# users/forms.py
from django import forms
from django.contrib.auth import forms as auth_forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from usuarios.models import Usuarios, Rol

class UsuariosCrearForma(UserCreationForm):
    
    def __init__(self, *args, **kwargs):
        super(UsuariosCrearForma, self).__init__(*args, **kwargs)
        self.fields['rol'].queryset = Rol.objects.exclude(rol='Administrador')
        

    class Meta:
        model = Usuarios
        fields = ('username', 'email', 'first_name', 'last_name', 'almacen','rol')


class UsuariosModificarForma(forms.ModelForm):
    password = auth_forms.ReadOnlyPasswordHashField(label="Password",
        help_text="No es posible visualizar como texto plano el password "
                  " del usuario, sin embargo este se puede cambiar usando el "
                  "siguiente link <a href=\"password/\">Cambiar Password</a>.")

    class Meta:
        model = Usuarios
        fields = ('username', 'email', 'first_name', 'last_name', 'almacen','rol')

    def __init__(self, *args, **kwargs):
        super(UserChangeForm, self).__init__(*args, **kwargs)
        f = self.fields.get('user_permissions', None)
        if f is not None:
            f.queryset = f.queryset.select_related('content_type')

    def clean_password(self):
        return self.initial["password"]