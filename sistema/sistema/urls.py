"""sistema URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include, url
#from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required, permission_required
from usuarios.views import UsuariosListado, UsuariosDetalle, UsuariosCrear, UsuariosActualizar, UsuariosEliminar, change_password, MiUsuariosActualizar, AlmacenListado, AlmacenDetalle, AlmacenCrear, AlmacenActualizar, AlmacenEliminar, RolActualizar, RolCrear, RolDetalle, RolEliminar, RolListado
#from . import views

#from Usuarioss.views import UsuariosList
from django.conf import settings
from django.conf.urls.static import static 

urlpatterns = [
    path('admin/', admin.site.urls),
    path('usuarios/', include('django.contrib.auth.urls')),
    path('lstusuarios', login_required(UsuariosListado.as_view(template_name = "usuarios/lstusuarios.html"),login_url='/usuarios/login/'), name='lstusuarios'),
    path('lstalmacenes', login_required(AlmacenListado.as_view(template_name = "almacenes/lstalmacenes.html"),login_url='/usuarios/login/'), name='lstalmacenes'),
    path('lstroles', login_required(RolListado.as_view(template_name = "roles/lstroles.html"),login_url='/usuarios/login/'), name='lstroles'),
    path('mainmenu', login_required(UsuariosListado.as_view(template_name = "usuarios/mainmenu.html"),login_url='/usuarios/login/'), name='mainmenu'),
    path('', UsuariosListado.as_view(template_name = "usuarios/inicio.html"), name='portada'),
    path('detalle/<int:pk>', login_required(UsuariosDetalle.as_view(template_name = "usuarios/detalles.html"),login_url='/usuarios/login/'), name='detalles'),
    path('almacen/detalle/<int:pk>', login_required(AlmacenDetalle.as_view(template_name = "almacenes/detalles.html"),login_url='/usuarios/login/'), name='almdetalles'),
    path('roles/detalle/<int:pk>', login_required(RolDetalle.as_view(template_name = "roles/detalles.html"),login_url='/usuarios/login/'), name='roldetalles'), 
    path('midetalle/<int:pk>', login_required(UsuariosDetalle.as_view(template_name = "usuarios/misdetalles.html"),login_url='/usuarios/login/'), name='misdetalles'), 
    path('crear', login_required(UsuariosCrear.as_view(template_name = "usuarios/crear.html"),login_url='/usuarios/login'), name='crear'),
    path('almacen/crear', login_required(AlmacenCrear.as_view(template_name = "almacenes/crear.html"),login_url='/usuarios/login'), name='almcrear'),
    path('roles/crear', login_required(RolCrear.as_view(template_name = "roles/crear.html"),login_url='/usuarios/login'), name='rolcrear'),
    path('editar/<int:pk>', login_required(UsuariosActualizar.as_view(template_name = "usuarios/actualizar.html"),login_url='/usuarios/login/'), name='actualizar'),
    path('almacen/editar/<int:pk>', login_required(AlmacenActualizar.as_view(template_name = "almacenes/actualizar.html"),login_url='/usuarios/login/'), name='almactualizar'),
    path('roles/editar/<int:pk>', login_required(RolActualizar.as_view(template_name = "roles/actualizar.html"),login_url='/usuarios/login/'), name='rolctualizar'),
    path('mieditar/<int:pk>', login_required(MiUsuariosActualizar.as_view(template_name = "usuarios/miactualizar.html"),login_url='/usuarios/login/'), name='miactualizar'),
    path('eliminar/<int:pk>', login_required(UsuariosEliminar.as_view(),redirect_field_name = '/lstusuarios'), name='eliminar'),
    path('almacen/eliminar/<int:pk>', login_required(AlmacenEliminar.as_view(),redirect_field_name = '/lstalmacenes'), name='almeliminar'),
    path('roles/eliminar/<int:pk>', login_required(RolEliminar.as_view(),redirect_field_name = '/lstroles'), name='roleliminar'),
    url(r'^password/$', change_password, name='change_password'),
]