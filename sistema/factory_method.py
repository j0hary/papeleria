from __future__ import generators
import random

class UsuarioPrueba(object):
    # Create based on class name:
    def factory(type):
        #return eval(type + "()")
        if type == "UsuarioAdmin": return UsuarioAdmin()
        if type == "UsuarioNormal": return UsuarioNormal()
        assert 0, "Bad shape creation: " + type
    factory = staticmethod(factory)

class UsuarioAdmin(UsuarioPrueba):
    def crear(self): print("Circle.draw")
    def complementar(self): print("Circle.erase")

class UsuarioNormal(UsuarioPrueba):
    def crear(self): print("Square.draw")
    def complementar(self): print("Square.erase")

# Generate shape name strings:
def GeneradorUsuariosPrueba(n):
    UserList = []
    #se crea un usuario administrador
    UserList.append(UsuarioPrueba.factory("UsuarioAdmin"))
    #se crean N usuarios normales
    for user in range(n):
        UserList.append(UsuarioPrueba.factory("UsuarioNormal"))
    for usuarioprueba in UserList:
        usuarioprueba.crear()
        usuarioprueba.complementar()



GeneradorUsuariosPrueba(5)




#############
'''
    def asigna_campos_adicionales(self):
        ALMACENESTEST = ['Morelia','Lazaro Cardenas','Zitacuaro']
        ROLESTEST= ['Administrador','Gerente','Jefe de Almacén','Auxiliar de Almacén']
        usuario = self.factory_method()
        usuario.almacen = ALMACENESTEST[randint(0,2)]
        usuario.rol = ROLESTEST[randint(0,3)]
        usuario.save()

class Crear_Usuario_Admin(Creador):
    def factory_method(self):
        return ObjUsuario_Admin()

class Crear_Usuario_Normal(Creador):
    def factory_method(self):
        return ObjUsuario_Normal()

class ObjUsuario_Admin(Usuarios):
    def creacion(self):
        user = Usuarios.objects.create_superuser('admin', email='admin@simiconti.com', password='t35tpa55admin')
        return 'Se ha creado el usuario Administrador: ',user,' con password: ','t35tpa55admin'

class ObjUsuario_Normal(Usuarios):
    def creacion(self,usnum):
        user = Usuarios.objects.create_user('ustest'+str(usnum), email='usuario'+str(usnum)+'@simiconti.com', password='t35tpa55ustest'+str(usnum))
        return 'Se ha creado el usuario: ',user,' con password: ','t35tpa55'+str(usnum)
'''