-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: papeleria2
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `papeleria2`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `papeleria2` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `papeleria2`;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
INSERT INTO `auth_group` VALUES (1,'Administrador');
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
INSERT INTO `auth_group_permissions` VALUES (2,1,5),(3,1,6),(4,1,7),(5,1,8),(6,1,13),(7,1,14),(8,1,15),(9,1,16),(10,1,29),(11,1,30),(12,1,31),(1,1,32);
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add permission',1,'add_permission'),(2,'Can change permission',1,'change_permission'),(3,'Can delete permission',1,'delete_permission'),(4,'Can view permission',1,'view_permission'),(5,'Can add group',2,'add_group'),(6,'Can change group',2,'change_group'),(7,'Can delete group',2,'delete_group'),(8,'Can view group',2,'view_group'),(9,'Can add content type',3,'add_contenttype'),(10,'Can change content type',3,'change_contenttype'),(11,'Can delete content type',3,'delete_contenttype'),(12,'Can view content type',3,'view_contenttype'),(13,'Can add user',4,'add_usuarios'),(14,'Can change user',4,'change_usuarios'),(15,'Can delete user',4,'delete_usuarios'),(16,'Can view user',4,'view_usuarios'),(17,'Can add logged in user',5,'add_loggedinuser'),(18,'Can change logged in user',5,'change_loggedinuser'),(19,'Can delete logged in user',5,'delete_loggedinuser'),(20,'Can view logged in user',5,'view_loggedinuser'),(21,'Can add log entry',6,'add_logentry'),(22,'Can change log entry',6,'change_logentry'),(23,'Can delete log entry',6,'delete_logentry'),(24,'Can view log entry',6,'view_logentry'),(25,'Can add session',7,'add_session'),(26,'Can change session',7,'change_session'),(27,'Can delete session',7,'delete_session'),(28,'Can view session',7,'view_session'),(29,'Can add almacen',8,'add_almacen'),(30,'Can change almacen',8,'change_almacen'),(31,'Can delete almacen',8,'delete_almacen'),(32,'Can view almacen',8,'view_almacen'),(33,'Can add rol',9,'add_rol'),(34,'Can change rol',9,'change_rol'),(35,'Can delete rol',9,'delete_rol'),(36,'Can view rol',9,'view_rol');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext CHARACTER SET utf8 COLLATE utf8_spanish_ci,
  `object_repr` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_usuarios_usuarios_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_usuarios_usuarios_id` FOREIGN KEY (`user_id`) REFERENCES `usuarios_usuarios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2019-11-15 07:09:44.353715','1','Administrador',1,'[{\"added\": {}}]',2,1),(2,'2019-11-15 07:11:13.453628','4','john_doe',2,'[]',4,1),(3,'2019-11-15 07:12:13.738352','3','olopez',2,'[{\"changed\": {\"fields\": [\"groups\"]}}]',4,1),(4,'2019-12-06 14:50:59.231210','1','admin',2,'[{\"changed\": {\"fields\": [\"last_name\"]}}]',4,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `model` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (6,'admin','logentry'),(2,'auth','group'),(1,'auth','permission'),(3,'contenttypes','contenttype'),(7,'sessions','session'),(8,'usuarios','almacen'),(5,'usuarios','loggedinuser'),(9,'usuarios','rol'),(4,'usuarios','usuarios');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2019-11-14 08:37:12.765471'),(2,'contenttypes','0002_remove_content_type_name','2019-11-14 08:37:12.860605'),(3,'auth','0001_initial','2019-11-14 08:37:12.960716'),(4,'auth','0002_alter_permission_name_max_length','2019-11-14 08:37:13.241102'),(5,'auth','0003_alter_user_email_max_length','2019-11-14 08:37:13.250097'),(6,'auth','0004_alter_user_username_opts','2019-11-14 08:37:13.259105'),(7,'auth','0005_alter_user_last_login_null','2019-11-14 08:37:13.272101'),(8,'auth','0006_require_contenttypes_0002','2019-11-14 08:37:13.278082'),(9,'auth','0007_alter_validators_add_error_messages','2019-11-14 08:37:13.289077'),(10,'auth','0008_alter_user_username_max_length','2019-11-14 08:37:13.299088'),(11,'auth','0009_alter_user_last_name_max_length','2019-11-14 08:37:13.308080'),(12,'auth','0010_alter_group_name_max_length','2019-11-14 08:37:13.391017'),(13,'auth','0011_update_proxy_permissions','2019-11-14 08:37:13.409009'),(14,'usuarios','0001_initial','2019-11-14 08:37:13.572508'),(15,'admin','0001_initial','2019-11-14 08:42:23.873378'),(16,'admin','0002_logentry_remove_auto_add','2019-11-14 08:42:24.017532'),(17,'admin','0003_logentry_add_action_flag_choices','2019-11-14 08:42:24.029525'),(18,'sessions','0001_initial','2019-11-14 08:42:24.067506'),(19,'usuarios','0002_auto_20191114_1551','2019-11-14 21:52:48.680995'),(20,'usuarios','0003_auto_20191114_1743','2019-11-14 23:43:51.709555'),(21,'usuarios','0004_usuarios_rol','2019-11-15 04:59:12.303401'),(22,'usuarios','0005_auto_20191125_1343','2019-11-25 19:48:18.414818'),(23,'usuarios','0006_rol','2019-12-06 14:25:33.259539'),(24,'usuarios','0007_auto_20191206_0833','2019-12-06 14:35:39.980619');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `session_data` longtext CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_almacen`
--

DROP TABLE IF EXISTS `usuarios_almacen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios_almacen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `almacen` varchar(32) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuarios_almacen_almacen_7f850daa_uniq` (`almacen`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_almacen`
--

LOCK TABLES `usuarios_almacen` WRITE;
/*!40000 ALTER TABLE `usuarios_almacen` DISABLE KEYS */;
INSERT INTO `usuarios_almacen` VALUES (6,'Jiquilpan'),(2,'Lazaro Cardenas'),(1,'Morelia'),(5,'Zamora'),(3,'Zitacuaro');
/*!40000 ALTER TABLE `usuarios_almacen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_loggedinuser`
--

DROP TABLE IF EXISTS `usuarios_loggedinuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios_loggedinuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_key` varchar(32) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `tiempo_expira` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `usuarios_loggedinuser_user_id_44ef0de7_fk_usuarios_usuarios_id` FOREIGN KEY (`user_id`) REFERENCES `usuarios_usuarios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_loggedinuser`
--

LOCK TABLES `usuarios_loggedinuser` WRITE;
/*!40000 ALTER TABLE `usuarios_loggedinuser` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios_loggedinuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_rol`
--

DROP TABLE IF EXISTS `usuarios_rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios_rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rol` varchar(32) COLLATE utf8_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rol` (`rol`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_rol`
--

LOCK TABLES `usuarios_rol` WRITE;
/*!40000 ALTER TABLE `usuarios_rol` DISABLE KEYS */;
INSERT INTO `usuarios_rol` VALUES (1,'Administrador'),(4,'Auxiliar de Almacén'),(2,'Gerente'),(3,'Jefe de Almacén');
/*!40000 ALTER TABLE `usuarios_rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_usuarios`
--

DROP TABLE IF EXISTS `usuarios_usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios_usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `first_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `last_name` varchar(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(254) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  `almacen_id` int(11) NOT NULL,
  `rol_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `usuarios_usuarios_almacen_id_af38a4d6` (`almacen_id`),
  KEY `usuarios_usuarios_rol_id_120e55eb` (`rol_id`),
  CONSTRAINT `usuarios_usuarios_almacen_id_af38a4d6_fk_usuarios_almacen_id` FOREIGN KEY (`almacen_id`) REFERENCES `usuarios_almacen` (`id`),
  CONSTRAINT `usuarios_usuarios_rol_id_120e55eb_fk_usuarios_rol_id` FOREIGN KEY (`rol_id`) REFERENCES `usuarios_rol` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_usuarios`
--

LOCK TABLES `usuarios_usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios_usuarios` DISABLE KEYS */;
INSERT INTO `usuarios_usuarios` VALUES (1,'pbkdf2_sha256$150000$JVSOSxVwegmF$m4oSbhxW8b82pjVhIZ48cx/NvTOYs+2R8ymDpoB9O8s=','2019-12-06 19:26:14.217068',1,'admin','administrador','del sistema','admin@simiconti.com',1,1,'2019-11-14 08:39:12.000000',1,1),(3,'pbkdf2_sha256$150000$I7EZg4K1VxLm$C0qjuUYLyGeGeAC7VQd0YIngcIGeIkf8byVQiIWDZik=','2019-12-06 14:36:56.839922',0,'olopez','Omar','Lopez Barcenas','omar@lopez.com',0,1,'2019-11-15 06:49:57.000000',2,3),(4,'pbkdf2_sha256$150000$ZAUgJoNWhg3j$bT+DJHdX5G/d1kp5fOU/0jt6kr4PXRdV8QG0fs/hcB4=','2019-11-29 19:05:32.775864',0,'john_doe','John','Doe','john@doe.com',0,1,'2019-11-15 06:58:32.000000',3,4),(5,'pbkdf2_sha256$150000$RI0yMV3udhJb$MfayJnrY2wifa9AWnL5kc27qD7zYoeevRquA3i1K5iE=',NULL,0,'jane_doe','Jane','Doe','jane@doe.com',0,1,'2019-11-15 06:59:44.785813',2,4),(6,'pbkdf2_sha256$150000$yGp2BAU9WZ02$QWq4U47UP3sGWfhpU9kNXC3Gg7vmc5KUVqrF+thG8fs=',NULL,0,'fulanito','Fulano','De tal','ful@nito.com',0,1,'2019-11-29 18:43:39.320074',1,2),(7,'pbkdf2_sha256$150000$gh4GjmAtfJSn$AVG1cZlpgbXHcJEjX+A4ews9XgIwa9OmmBz3Ez43jhw=',NULL,0,'ustest22','Usuario','Para pruebas de Borrado','usuario2@simiconti.com',0,1,'2019-12-04 16:54:58.792439',1,4);
/*!40000 ALTER TABLE `usuarios_usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_usuarios_groups`
--

DROP TABLE IF EXISTS `usuarios_usuarios_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios_usuarios_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuarios_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuarios_usuarios_groups_usuarios_id_group_id_31056d4d_uniq` (`usuarios_id`,`group_id`),
  KEY `usuarios_usuarios_groups_group_id_1e265f46_fk_auth_group_id` (`group_id`),
  CONSTRAINT `usuarios_usuarios_gr_usuarios_id_65c166be_fk_usuarios_` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios_usuarios` (`id`),
  CONSTRAINT `usuarios_usuarios_groups_group_id_1e265f46_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_usuarios_groups`
--

LOCK TABLES `usuarios_usuarios_groups` WRITE;
/*!40000 ALTER TABLE `usuarios_usuarios_groups` DISABLE KEYS */;
INSERT INTO `usuarios_usuarios_groups` VALUES (1,3,1);
/*!40000 ALTER TABLE `usuarios_usuarios_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_usuarios_user_permissions`
--

DROP TABLE IF EXISTS `usuarios_usuarios_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios_usuarios_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuarios_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuarios_usuarios_user_p_usuarios_id_permission_i_1fb72da5_uniq` (`usuarios_id`,`permission_id`),
  KEY `usuarios_usuarios_us_permission_id_394f07a6_fk_auth_perm` (`permission_id`),
  CONSTRAINT `usuarios_usuarios_us_permission_id_394f07a6_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `usuarios_usuarios_us_usuarios_id_d860a7b5_fk_usuarios_` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios_usuarios` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_usuarios_user_permissions`
--

LOCK TABLES `usuarios_usuarios_user_permissions` WRITE;
/*!40000 ALTER TABLE `usuarios_usuarios_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios_usuarios_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Current Database: `test_papeleria2`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `test_papeleria2` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `test_papeleria2`;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can view log entry',1,'view_logentry'),(5,'Can add permission',2,'add_permission'),(6,'Can change permission',2,'change_permission'),(7,'Can delete permission',2,'delete_permission'),(8,'Can view permission',2,'view_permission'),(9,'Can add group',3,'add_group'),(10,'Can change group',3,'change_group'),(11,'Can delete group',3,'delete_group'),(12,'Can view group',3,'view_group'),(13,'Can add content type',4,'add_contenttype'),(14,'Can change content type',4,'change_contenttype'),(15,'Can delete content type',4,'delete_contenttype'),(16,'Can view content type',4,'view_contenttype'),(17,'Can add session',5,'add_session'),(18,'Can change session',5,'change_session'),(19,'Can delete session',5,'delete_session'),(20,'Can view session',5,'view_session'),(21,'Can add user',6,'add_usuarios'),(22,'Can change user',6,'change_usuarios'),(23,'Can delete user',6,'delete_usuarios'),(24,'Can view user',6,'view_usuarios'),(25,'Can add logged in user',7,'add_loggedinuser'),(26,'Can change logged in user',7,'change_loggedinuser'),(27,'Can delete logged in user',7,'delete_loggedinuser'),(28,'Can view logged in user',7,'view_loggedinuser'),(29,'Can add almacen',8,'add_almacen'),(30,'Can change almacen',8,'change_almacen'),(31,'Can delete almacen',8,'delete_almacen'),(32,'Can view almacen',8,'view_almacen'),(33,'Can add rol',9,'add_rol'),(34,'Can change rol',9,'change_rol'),(35,'Can delete rol',9,'delete_rol'),(36,'Can view rol',9,'view_rol');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_usuarios_usuarios_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_usuarios_usuarios_id` FOREIGN KEY (`user_id`) REFERENCES `usuarios_usuarios` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'contenttypes','contenttype'),(5,'sessions','session'),(8,'usuarios','almacen'),(7,'usuarios','loggedinuser'),(9,'usuarios','rol'),(6,'usuarios','usuarios');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2019-12-06 19:33:44.443713'),(2,'contenttypes','0002_remove_content_type_name','2019-12-06 19:33:50.453659'),(3,'auth','0001_initial','2019-12-06 19:33:53.063637'),(4,'auth','0002_alter_permission_name_max_length','2019-12-06 19:34:06.394130'),(5,'auth','0003_alter_user_email_max_length','2019-12-06 19:34:06.556059'),(6,'auth','0004_alter_user_username_opts','2019-12-06 19:34:06.643781'),(7,'auth','0005_alter_user_last_login_null','2019-12-06 19:34:06.753580'),(8,'auth','0006_require_contenttypes_0002','2019-12-06 19:34:07.003663'),(9,'auth','0007_alter_validators_add_error_messages','2019-12-06 19:34:07.094210'),(10,'auth','0008_alter_user_username_max_length','2019-12-06 19:34:07.263587'),(11,'auth','0009_alter_user_last_name_max_length','2019-12-06 19:34:07.373672'),(12,'auth','0010_alter_group_name_max_length','2019-12-06 19:34:07.953946'),(13,'auth','0011_update_proxy_permissions','2019-12-06 19:34:08.049008'),(14,'usuarios','0001_initial','2019-12-06 19:34:10.483631'),(15,'admin','0001_initial','2019-12-06 19:34:27.704031'),(16,'admin','0002_logentry_remove_auto_add','2019-12-06 19:34:35.523715'),(17,'admin','0003_logentry_add_action_flag_choices','2019-12-06 19:34:35.743575'),(18,'sessions','0001_initial','2019-12-06 19:34:36.953673'),(19,'usuarios','0002_auto_20191114_1551','2019-12-06 19:34:41.293993'),(20,'usuarios','0003_auto_20191114_1743','2019-12-06 19:34:46.324123'),(21,'usuarios','0004_usuarios_rol','2019-12-06 19:34:47.343888'),(22,'usuarios','0005_auto_20191125_1343','2019-12-06 19:34:48.473693'),(23,'usuarios','0006_rol','2019-12-06 19:34:50.143704'),(24,'usuarios','0007_auto_20191206_0833','2019-12-06 19:35:13.083789');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_almacen`
--

DROP TABLE IF EXISTS `usuarios_almacen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios_almacen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `almacen` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuarios_almacen_almacen_7f850daa_uniq` (`almacen`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_almacen`
--

LOCK TABLES `usuarios_almacen` WRITE;
/*!40000 ALTER TABLE `usuarios_almacen` DISABLE KEYS */;
INSERT INTO `usuarios_almacen` VALUES (5,'Jiquilpan'),(2,'Lazaro Cardenas'),(1,'Morelia'),(4,'Zamora'),(3,'Zitacuaro');
/*!40000 ALTER TABLE `usuarios_almacen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_loggedinuser`
--

DROP TABLE IF EXISTS `usuarios_loggedinuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios_loggedinuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_key` varchar(32) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `tiempo_expira` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `usuarios_loggedinuser_user_id_44ef0de7_fk_usuarios_usuarios_id` FOREIGN KEY (`user_id`) REFERENCES `usuarios_usuarios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_loggedinuser`
--

LOCK TABLES `usuarios_loggedinuser` WRITE;
/*!40000 ALTER TABLE `usuarios_loggedinuser` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios_loggedinuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_rol`
--

DROP TABLE IF EXISTS `usuarios_rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios_rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rol` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rol` (`rol`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_rol`
--

LOCK TABLES `usuarios_rol` WRITE;
/*!40000 ALTER TABLE `usuarios_rol` DISABLE KEYS */;
INSERT INTO `usuarios_rol` VALUES (1,'Administrador'),(3,'Auxiliar de Almacén'),(2,'Gerente'),(4,'Jefe de Almacén');
/*!40000 ALTER TABLE `usuarios_rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_usuarios`
--

DROP TABLE IF EXISTS `usuarios_usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios_usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  `almacen_id` int(11) NOT NULL,
  `rol_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  KEY `usuarios_usuarios_almacen_id_af38a4d6` (`almacen_id`),
  KEY `usuarios_usuarios_rol_id_120e55eb` (`rol_id`),
  CONSTRAINT `usuarios_usuarios_almacen_id_af38a4d6_fk_usuarios_almacen_id` FOREIGN KEY (`almacen_id`) REFERENCES `usuarios_almacen` (`id`),
  CONSTRAINT `usuarios_usuarios_rol_id_120e55eb_fk_usuarios_rol_id` FOREIGN KEY (`rol_id`) REFERENCES `usuarios_rol` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_usuarios`
--

LOCK TABLES `usuarios_usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios_usuarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios_usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_usuarios_groups`
--

DROP TABLE IF EXISTS `usuarios_usuarios_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios_usuarios_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuarios_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuarios_usuarios_groups_usuarios_id_group_id_31056d4d_uniq` (`usuarios_id`,`group_id`),
  KEY `usuarios_usuarios_groups_group_id_1e265f46_fk_auth_group_id` (`group_id`),
  CONSTRAINT `usuarios_usuarios_gr_usuarios_id_65c166be_fk_usuarios_` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios_usuarios` (`id`),
  CONSTRAINT `usuarios_usuarios_groups_group_id_1e265f46_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_usuarios_groups`
--

LOCK TABLES `usuarios_usuarios_groups` WRITE;
/*!40000 ALTER TABLE `usuarios_usuarios_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios_usuarios_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios_usuarios_user_permissions`
--

DROP TABLE IF EXISTS `usuarios_usuarios_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios_usuarios_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuarios_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuarios_usuarios_user_p_usuarios_id_permission_i_1fb72da5_uniq` (`usuarios_id`,`permission_id`),
  KEY `usuarios_usuarios_us_permission_id_394f07a6_fk_auth_perm` (`permission_id`),
  CONSTRAINT `usuarios_usuarios_us_permission_id_394f07a6_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `usuarios_usuarios_us_usuarios_id_d860a7b5_fk_usuarios_` FOREIGN KEY (`usuarios_id`) REFERENCES `usuarios_usuarios` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios_usuarios_user_permissions`
--

LOCK TABLES `usuarios_usuarios_user_permissions` WRITE;
/*!40000 ALTER TABLE `usuarios_usuarios_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios_usuarios_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-06 13:49:14
